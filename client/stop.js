var stopAll = document.getElementById('stop-recording');
var startAll = document.getElementById('start-recording');

function stoping(){
    stopAll.click();
}

function starting(){
    startAll.click();
}

async function checkCameraAudio() {
    console.log('Inside CheckCameraAudio function');
    var permit = true;
    
    await navigator.permissions.query({name: 'microphone'}).then((permissionObj) => {
        console.log('Microphone State = ',permissionObj.state);
        if(permissionObj.state != 'granted') {
            permit = false;
        }
    })
    .catch((error) => {
        console.log('Got error :', error);
        permit = false;
    })

    await navigator.permissions.query({name: 'camera'}).then((permissionObj) => {
        console.log('Camera State = ',permissionObj.state);
        if(permissionObj.state != 'granted') {
            permit = false;
        }
    })
    .catch((error) => {
        console.log('Got error :', error);
        permit = false;
    })
    console.log('Final Permit Value: ',permit);
    return permit;
    
}
