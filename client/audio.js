const start = document.getElementById('start-recording');
const stop = document.getElementById('stop-recording');
var mediaRecorderA;
var audiosContainer = document.getElementById('audios-container');
var index = 1;
var uploader;

async function captureUserMedia1(mediaconstraints) {
    navigator.mediaDevices.getUserMedia(mediaconstraints).then(onMediaSuccess1).catch(onMediaError);
}

start.addEventListener("click", function (ev) {
    socket.on('startDown', function () {
        uploader = new SocketIOFileClient(socket);
        captureUserMedia1({
            audio: true
        });
    });
}, false);

stop.addEventListener('click', function (ev) {
    //this.disabled = true;
    mediaRecorderA.stop();
    mediaRecorderA.stream.stop();
}, false);

async function onMediaSuccess1(stream) {
    var audio = document.createElement('audio');
    audio = mergeProps(audio, {
        controls: true,
        muted: true
    });
    audio.srcObject = stream;
    audio.play();
    audiosContainer.appendChild(audio);
    audiosContainer.appendChild(document.createElement('hr'));
    mediaRecorderA = new MediaStreamRecorder(stream);
    mediaRecorderA.stream = stream;
    /*var recorderType = document.getElementById('audio-recorderType');
      don't force any mimeType; use above "recorderType" instead.
     mediaRecorderA.mimeType = 'audio/webm'; // audio/ogg or audio/wav or audio/webm
    mediaRecorderA.audioChannels = !!document.getElementById('left-channel').checked ? 1 : 2;*/
    mediaRecorderA.ondataavailable = function (blob) {
        var src = Date.now() + ".wav";
        var file = new File([blob], src,
            {
                type: 'audio/wav',
                lastModified: new Date()
            });
        console.info(file);
        uploadaudio(file);
    };
    mediaRecorderA.start(timeInterval);
}

function uploadaudio(file) {
    console.log("It went");
    setTimeout(function () {
        var uploadIds = uploader.upload([file], { uploadTo: 'audio' });
    }, 6000);
};

function onMediaError(e) {
    stoping();
    console.log("Yeros");
    console.error('media error', e);
}

function bytesToSize(bytes) {
    var k = 1000;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Bytes';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(k)), 10);
    return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}

function getTimeLength(milliseconds) {
    var data = new Date(milliseconds);
    return data.getUTCHours() + " hours, " + data.getUTCMinutes() + " minutes and " + data.getUTCSeconds() + " second(s)";
}

