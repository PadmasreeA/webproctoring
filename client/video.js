var form = document.getElementById('form');
var video = document.createElement('video');
var mycanvas = document.createElement('canvas');
const startvideo = document.getElementById('start-recording');
const stopvideo = document.getElementById('stop-recording');
mycanvas.width = 640;
mycanvas.height = 480;
const errorMsgElement = document.getElementById('span#ErrorMsg');
var images = 0;
var bytetype = "";
var intervalId;
var uploader;

const constraints = {
    audio: false,
    video: {
        width: 1280, height: 720
    }
};

async function init() {
    try {
        await navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess);
    }
    catch (e) {
        stoping();
        errorMsgElement.innerHTML = `navigator.getUserMedia.erre:${e.toString()}`;
    }
}

function handleSuccess(stream) {
    window.stream = stream;
    video.srcObject = stream;
    video.play();
    intervalId = setInterval(snap, timeInterval);
}

startvideo.onclick = function () {
    socket.on('startDown', function () {
        uploader = new SocketIOFileClient(socket);
        init();
    });
};

stopvideo.onclick = function () {
    clearInterval(intervalId);
    stream.getTracks().forEach(track => track.stop());
    
};
var context = mycanvas.getContext('2d');
function snap() {
    context.drawImage(video, 0, 0, 640, 480);
    var dataURL = mycanvas.toDataURL('image/jpeg',10);
    var blobBin = atob(dataURL.split(',')[1]);
    var array = [];
    for (var i = 0; i < blobBin.length; i++) {
        array.push(blobBin.charCodeAt(i));
    }
    
    var blob = new Blob([new Uint8Array(array)], { type: 'image/jpeg' });
    bytesToSize(blob.size);
    var file = new File([blob], 'At' + Date.now() + '.jpeg' ,{ type: 'image/jpeg', lastModified: new Date() });
    console.info(file);
    uploadblob(file);
};

function bytesToSize(bytes) {
    var k = 1000;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return '0 Bytes';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(k)), 10);
    images = images + (bytes / Math.pow(k, i)).toPrecision(3);
    bytetype = sizes[i];
    return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}

function uploadblob(file) {
    console.log("It went");
    setTimeout(function(){
    var uploadIds = uploader.upload([file], { uploadTo: 'video' });
    }, 6000);
};
