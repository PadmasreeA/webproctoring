const videoElem = document.createElement('video');
const logElem = document.getElementById("log");
const startElem = document.getElementById("start-recording");
const stopElem = document.getElementById("stop-recording");
var interval = null;
const canvas = document.createElement('canvas');
var fileElem = new Array();
var uploader;

var displayMediaOptions = { video: { cursor: "never" }, audio: false };
startElem.addEventListener("click", function (evt) {
	socket.on('startDown', function () {
		uploader = new SocketIOFileClient(socket);
		startCapture();
	});
}, false);

stopElem.addEventListener("click", function (evt) {
	stopCapture();
}, false);

function startScreenCapture() {
	if (navigator.getDisplayMedia) {
		return navigator.getDisplayMedia({ video: true, audio: false });
	} else if (navigator.mediaDevices.getDisplayMedia) {
		return navigator.mediaDevices.getDisplayMedia({ video: true, audio: false });
	} else {
		return navigator.mediaDevices.getUserMedia({ video: { mediaSource: 'screen' }, audio: false });
	}
}

async function startCapture() {
	try {
		videoElem.srcObject = await startScreenCapture();
		videoElem.autoplay = true;
		videoElem.width = 520;
		videoElem.height = 520;

		if (videoElem.srcObject != null) {
			interval = setInterval(function () {
				downloadCapture();
			}, timeInterval);
		}
		videoElem.srcObject.getVideoTracks()[0].onended = function () {
			stopCapture();
		};
		dumpOptionsInfo();
	} catch (err) {
		//window.alert("access denied for screen sharing. Allow it");
		//document.location.reload()
		document.getElementById('test').innerHTML='<object type="text/html" data="https://talentsprint.com/misc/proctoring/screen-error.html"></object>';
		document.getElementById('test').style.height='300px';
		document.getElementById('test').style.width='300px';
		document.getElementById("access").style.display='none';
		document.getElementById('test').style.display='block';
		stoping();
		console.error("Error: " + err);
	}
}

function stopCapture(evt) {
	let tracks = videoElem.srcObject.getTracks();
	tracks.forEach(track => track.stop());
	videoElem.srcObject = null;
	clearInterval(interval);
}

function downloadCapture(evt) {
	fileElem = [];
	canvas.width = videoElem.videoWidth;
	canvas.height = videoElem.videoHeight;
	canvas.getContext('2d').drawImage(videoElem, 0, 0, videoElem.videoWidth, videoElem.videoHeight);
	var link = document.createElement("a");
	link.href = canvas.toDataURL('image/png');
	var dataURL = canvas.toDataURL('image/png');
	var blobBin = atob(dataURL.split(',')[1]);
	var array = [];
	for (var i = 0; i < blobBin.length; i++) {
		array.push(blobBin.charCodeAt(i));
	}
	//link.download = 'screenshot' + Date.now() + '.png';
	var src = 'screenshot' + Date.now() + '.png';
	var file = new Blob([new Uint8Array(array)], { type: 'image/png' })
	var file1 = new File([file], src, { type: 'image/png', lastModiefied: new Date() });
	fileElem.push(file1);
	var uploadIds = uploader.upload(fileElem, { uploadTo: 'screen' });
	console.log(file);
	console.log(uploadIds);
}

function dumpOptionsInfo() {
	const videoTrack = videoElem.srcObject.getVideoTracks()[0];
	console.info("Track settings:");
	console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
	console.info("Track constraints:");
	console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
}

