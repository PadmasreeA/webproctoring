var express = require('express');
var app = express();
var https = require('http');
const httpsServer = https.createServer(app);
var io = null;
const SocketIOFile = require('socket.io-file');
const router = express.Router();

app.get('/stop.js', (req, res, next) => {
	return res.sendFile(__dirname + '/client/stop.js');
});
app.get('/connection.js', (req, res, next) => {
	return res.sendFile(__dirname + '/client/connection.js');
});
app.get('/', (req, res, next) => {
	return res.sendFile(__dirname + '/client/blank.html');
});

app.get('/start_test.html', (req, res, next) => {
	return res.sendFile(__dirname + '/client/star_test.html');
});

app.get('/something.png',(req,res,next)=>{
	return res.sendFile(__dirname+'/data/person1577181557770/screen/screenshot1577181570357.png');
});
app.get('/screensharing.js', (req, res, next) => {
	return res.sendFile(__dirname + '/client/screensharing.js');
});

app.get('/audio.js', (req, res, next) => {
	return res.sendFile(__dirname + '/client/audio.js');
});
app.get('/video.js', (req, res, next) => {
	return res.sendFile(__dirname + '/client/video.js');
});


/*app.get('/app.js', (req, res, next) => {
	return res.sendFile(__dirname + '/client/app.js');
});*/

app.get('/audioSupport.js', (req, res, next) => {
	return res.sendFile(__dirname+'/client/MediaStreamRecorder.js');
})


app.get('/socket.io.js', (req, res, next) => {
	return res.sendFile(__dirname + '/node_modules/socket.io-client/dist/socket.io.js');
});

app.get('/socket.io-file-client.js', (req, res, next) => {
	return res.sendFile(__dirname + '/node_modules/socket.io-file-client/socket.io-file-client.js');
});

console.log("helllo");
var dir;
var msg;
var uploader;
io = require('socket.io')(httpsServer);
io.on('connection', (socket) => {
	console.log("socket connected");
	socket.on('startCon',function(msg) {
		console.log('message: ' + msg);
		msg = msg + "_" + Date.now();
		dir = 'data/' + msg;
		uploader = new SocketIOFile(socket, {
			uploadDir:  {audio: dir+'/audio', video: dir+'/video', screen: dir +'/screen'},  // simple directory
			// accepts: ['audio/mpeg', 'audio/mp3'],                // chrome and some of browsers checking mp3 as 'audio/mp3', not 'audio/mpeg'
			// maxFileSize: 4194304,                                                // 4 MB. default is undefined(no limit)
			chunkSize: 10240,                                                       // default is 10240(1KB)
			transmissionDelay: 0, // delay of each transmission, higher value saves more cpu resources, lower upload speed. default is 0(no delay)
			overwrite: false,               // overwrite file if exists, default is true.
		});
		socket.emit('startDown',msg);
		uploader.on('start', (fileInfo) => {
			console.log('Start uploading');
			console.log(fileInfo);
		});
		uploader.on('stream', (fileInfo) => {
			console.log(`${fileInfo.wrote} / ${fileInfo.size} byte(s)`);
		});
		uploader.on('complete', (fileInfo) => {
			console.log('Upload Complete.');
			console.log(fileInfo);
		});
		uploader.on('error', (err) => {
			console.log(msg);
			console.log('Error!', err);

		});
		uploader.on('abort', (fileInfo) => {
			console.log('Aborted: ', fileInfo);
		});
	});
});
httpsServer.listen(3000, () => {
	console.log('Server listening');
});
